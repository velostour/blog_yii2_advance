<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "entry".
 *
 * @property integer $id
 * @property string $text
 * @property string $title
 *
 * @property Entries $entries
 */
class Entry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'title'], 'required'],
            [['text'], 'string', 'max' => 1000 ],
            [['title'], 'string', 'max' => 255 ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntries()
    {
        return $this->hasOne(Entries::className(), ['entry_id' => 'id']);
    }
}
