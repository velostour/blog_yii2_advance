<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "authors".
 *
 * @property integer $id
 * @property string $mail
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Comments[] $comments
 * @property Entries[] $entries
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mail','name'], 'required'],
            [['mail'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mail' => 'Mail',
            'avatar' => 'Avatar',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['author' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntries()
    {
        return $this->hasMany(Entries::className(), ['author' => 'id']);
    }
    
    
    
    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source https://gravatar.com/site/implement/images/php/
     */
     public function getAvatar( $s = 65, $d = 'mm', $r = 'g', $img = false, $atts = [] ) {

         if ( $this->mail ) {
             $url = 'https://www.gravatar.com/avatar/';
             $url .= md5( strtolower( trim( $this->mail ) ) );
             $url .= "?s=$s&d=$d&r=$r";
             if ( $img ) {
                 $url = '<img src="' . $url . '"';
                 foreach ( $atts as $key => $val )
                     $url .= ' ' . $key . '="' . $val . '"';
                 $url .= ' />';
             }
         } else {
             $url = 'http://keenthemes.com/assets/bootsnipp/img1-small.jpg';
         }
        return $url;
    }
    
}
