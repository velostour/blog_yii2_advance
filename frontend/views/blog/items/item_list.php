<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Entries */

?>
    <div class="col-sm-12">
        <div id="tb-testimonial" class="testimonial testimonial-primary">
            <div class="testimonial-section">
                <?= \yii\helpers\StringHelper::truncateWords($model->entry->text, 25,'...') ?>
            </div>
            <div class="testimonial-desc">
                <img src="<?= Html::encode( $model->author0->getAvatar()) ; ?>" alt="" />
                <div class="testimonial-writer">
                    <div class="testimonial-writer-name"><?= Html::a( Html::encode( $model->entry->title), ['view', 'id' => $model->id]); ?></div>
                    <div class="testimonial-writer-name-small">Написал: <b><?= Html::encode( $model->author0->name)?></b>
                        <?= \Yii::$app->formatter->asDatetime( $model->created_at , "php:d.m.Y" ) ?>
                        в <?= \Yii::$app->formatter->asDatetime( $model->created_at , "php:H:i:s " ) ?></div>
                    <div class="testimonial-writer-name-small">Комментариев: <b><?= Html::encode( count($model->comments) )?></b></div>
                </div>
            </div>
        </div>
    </div>