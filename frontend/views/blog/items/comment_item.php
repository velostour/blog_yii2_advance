<?php
/**
 * Created by PhpStorm.
 * User: ЫРФТЛ
 * Date: 08.06.2016
 * Time: 20:19
 */

/* @var $this yii\web\View */
/* @var $model frontend\models\Comments */
/* @var $comment yii\widgets\ActiveForm */

?>

        <div class="col-md-12">
            <div class="testimonials">
                <div class="active item">

                    <div class="col-md-3">
                        <div class="carousel-info">
                            <img alt="" src="http://keenthemes.com/assets/bootsnipp/img1-small.jpg" class="pull-left">
                            <div class="pull-left">
                                <span class="testimonials-name"><?= $model->author0->name ?></span>
                                <span class="testimonials-post"><?= \Yii::$app->formatter->asDatetime( $model->created_at , "php:d.m.Y  H:i:s") ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <blockquote><p><?= $model->comment->text ?></p></blockquote>
                    </div>
                </div>
            </div>
        </div>